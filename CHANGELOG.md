# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.4] - 25/11/2021

- Add `core_version_requirement: ^9 || ^10` and remove `core: 8.x` from composer.

## [1.0.3] - 04/05/2021

- Fix translations for blocked videos text

## [1.0.2] - 19/04/2021

- Modify text of blocked embedded videos using JS

## [1.0.1] - 05/03/2021

- Split implementation between Matomo and Google

## [1.0.0] - 17/02/2021

- Add blocking for embedded videos from Youtube, CDS, Vimeo when user does not consent
